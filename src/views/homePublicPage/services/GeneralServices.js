import GeneralServicesDefinitions from './GeneralServicesDefinitions'
import Services from '../../../services/DefaultServices'

export default class GeneralServices extends Services {
  getAllProducts = () => {
    return this.get(GeneralServicesDefinitions.getAllProducts)
  }

  searchProducts = (data) => {
    return this.get(GeneralServicesDefinitions.searchProducts(data))
  }
}
