const API_PATH = 'http://localhost:1338/api/'
//const API_PATH = 'https://backstoretestbsale.herokuapp.com/api/'

const GeneralServicesDefinitions = Object.freeze({
  getAllProducts: `${API_PATH}page/get-all-products`,
  searchProducts: (word) => {
    return `${API_PATH}page/search-products/${word}`
  },
})

export default GeneralServicesDefinitions
