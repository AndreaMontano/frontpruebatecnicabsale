export default function sortingProducts(sortBy, productsToShow) {
  let products = [...productsToShow]

  switch (sortBy) {
    case 'ascendingPrice':
      products = products.sort((a, b) => a.price - b.price)

      break
    case 'descendingPrice':
      products = products.sort((a, b) => b.price - a.price)

      break

    case 'alphabeticalOrder':
      products = products.sort((a, b) =>
        a.name.toLowerCase() > b.name.toLowerCase()
          ? 1
          : a.name.toLowerCase() < b.name.toLowerCase()
          ? -1
          : 0
      )
      break
    default:
      products = [...productsToShow]
  }
  return products
}
