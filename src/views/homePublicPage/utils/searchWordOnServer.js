export default function searchWordOnserver() {
  const isSearching = localStorage.getItem('isSearching')
  const searchBy = localStorage.getItem('searchBy')

  return {
    isSearching,
    searchBy,
  }
}
