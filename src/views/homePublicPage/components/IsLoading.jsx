import React from 'react'
import Loader from 'react-loader-spinner'

export default function IsLoading(isLoading) {
  return (
    <div className={`center-align`}>
      {isLoading && (
        <Loader type='BallTriangle' color='#4DB6AC' height={100} width={100} />
      )}
    </div>
  )
}
