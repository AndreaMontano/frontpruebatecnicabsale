import React from 'react'
import Select from '../../../helpers/Select'

import { DivContainer } from '../styles/categories'

export default function Categories(props) {
  return (
    <DivContainer>
      {props.allCategories.length > 0 && (
        <Select
          placeholder={`seleccione una categoria`}
          name={`categoryName`}
          arrayOptions={props.allCategories}
          keyValueName={`id`}
          keyMessageName={`name`}
          onChange={props.selectCategorie}
          variant={'outlined'}
        />
      )}
    </DivContainer>
  )
}
