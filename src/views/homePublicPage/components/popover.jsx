import React from 'react'
import Popover from '@material-ui/core/Popover'

import Input from '../../../helpers/InputText'
import SearchIcon from '@material-ui/icons/Search'

export default function SimplePopover(props) {
  const [anchorEl, setAnchorEl] = React.useState(null)

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const open = Boolean(anchorEl)
  const id = open ? 'simple-popover' : undefined

  return (
    <div>
      <SearchIcon
        aria-describedby={id}
        variant='contained'
        color='primary'
        onClick={handleClick}
      />

      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <Input
          type={'text'}
          placeholder={'Ingrese una palabra'}
          onChange={props.searchWordOnServer}
        />
      </Popover>
    </div>
  )
}
