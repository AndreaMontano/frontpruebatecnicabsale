import React from 'react'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import Divider from '@material-ui/core/Divider'
import {
  DivContainer,
  DivTitle,
  EachProduct,
  DivPhoto,
  DivName,
  DivPrice,
  Price,
  CartIcon,
} from '../styles/productsToShowStyles'

export default function ProductsToShow(props) {
  const productsToShow = props.productsToShow ? props.productsToShow : []

  return (
    <DivContainer>
      <DivTitle>{`Productos disponibles`}</DivTitle>
      {productsToShow.length > 0 &&
        productsToShow.map((eachProduct) => (
          <EachProduct className={`card`}>
            <DivPhoto>
              <img alt='' src={eachProduct.url_image}></img>
            </DivPhoto>
            <DivName>{eachProduct.name}</DivName>
            <Divider orientation='horizontal' label='Hard' />
            <DivPrice>
              <Price>{`$ ${eachProduct.price}`}</Price>
              <CartIcon>
                <ShoppingCartIcon />
              </CartIcon>
            </DivPrice>
          </EachProduct>
        ))}
    </DivContainer>
  )
}
