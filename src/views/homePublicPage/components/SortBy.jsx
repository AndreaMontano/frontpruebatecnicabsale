import React from 'react'
import Select from '../../../helpers/Select'

import { DivContainer } from '../styles/categories'

export default function Categories(props) {
  const arrayOptions = [
    { id: 'ascendingPrice', message: 'Precio menor a mayor' },
    { id: 'descendingPrice', message: 'Precio mayor a menor' },
    { id: 'alphabeticalOrder', message: 'Orden alfabético' },
  ]
  return (
    <DivContainer>
      <Select
        placeholder={`ordenar `}
        name={`sortBy`}
        arrayOptions={arrayOptions}
        keyValueName={`id`}
        keyMessageName={`message`}
        onChange={props.sortBy}
        editingCategory={props.editingCategory}
      />
    </DivContainer>
  )
}
