import React from 'react'

import { DivContainer } from '../styles/notMatchedProdStyles'

export default function NotMatchedProducts(props) {
  return (
    <DivContainer>
      Lo sentimos. No hemos enconrtado coincidencias con el productos buscado.
    </DivContainer>
  )
}
