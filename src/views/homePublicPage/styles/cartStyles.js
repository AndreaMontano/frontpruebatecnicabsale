import styled from 'styled-components'

export const DivContainer = styled.div`
  height: 80px;
  display: flex;
  align-items: center;
  padding: 0 10%;
