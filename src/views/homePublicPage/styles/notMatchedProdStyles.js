import styled from 'styled-components'
import { Variables } from '../../../assets/styles/variables'

export const DivContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-around;
  padding: 10%;
  font-size: 1.2em;
  color: ${Variables.grayColor};
`
