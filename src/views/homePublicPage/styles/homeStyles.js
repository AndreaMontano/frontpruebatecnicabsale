import styled from 'styled-components'
import { Variables } from '../../../assets/styles/variables'

export const DivContainer = styled.div`
  width: 100%;
`

export const DivActions = styled.div`
  width: 100%;

  background-color: ${Variables.mediumGrayColor};

  @media (max-width: 599px) {
    width: 100% !important;
  }
  @media (min-width: 600px) {
    display: flex;
  }
`

