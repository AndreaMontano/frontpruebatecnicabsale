import styled from 'styled-components'
import { Variables } from '../../../assets/styles/variables'

export const DivContainer = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  padding: 0 10%;
`

export const DivTitle = styled.div`
  width: 100%;
  display: flex;
  text-transform: uppercase;
  color: ${Variables.mediumGrayColor};
  justify-content: space-around;
  padding: 2%;
  font-weight: bolder;
  font-size: 1.2em;
`

export const EachProduct = styled.div`
  width: 200px;
  height: 300px;
  margin: 1%;
`

export const DivPhoto = styled.div`
  width: 200px;
  height: 200px;
  display: flex;
  justify-content: space-around;
  img {
    max-width: 200px;
    max-height: 200px;
    padding: 5%;
  }
`

export const DivName = styled.div`
  width: 100%;
  padding: 0 5%;
  color: ${Variables.mediumGrayColor};
  height: 50px;
  display: flex;
  align-items: center;
`

export const DivPrice = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  padding: 0 10%;
  height: 50px;
`

export const Price = styled.div`
  width: 50%;
  color: ${Variables.mediumGrayColor};
`

export const CartIcon = styled.div`
  width: auto;
  display: flex;
  margin-left: auto;
  padding: 3%;
  background-color: ${Variables.mediumGrayColor};
  border-radius: 20px;
  svg {
    color: ${Variables.whiteColor};
  }
`
