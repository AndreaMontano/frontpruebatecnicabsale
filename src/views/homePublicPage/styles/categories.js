import styled from 'styled-components'

export const DivContainer = styled.div`
  height: 80px;
  display: flex;
  align-items: center;
  padding: 0 10%;

  @media (max-width: 599px) {
    width: 100%;
  }

  @media (min-width: 600px) {
    width: 50%;
  }
`
