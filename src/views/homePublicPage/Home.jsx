import React from 'react'

import { GeneralServices } from '../../services/index'
import { DivContainer, DivActions } from './styles/homeStyles'

import IsLoading from './components/IsLoading'
import Categories from './components/Categories'
import ProductsToShow from './components/ProductsToShow'
import SortBy from './components/SortBy'
import NotMatched from './components/NotMatchedProducts'

import sortingBy from './utils/sortingProducts'
import searchWordOnServer from './utils/searchWordOnServer'

export default class SingleView extends React.Component {
  constructor(props) {
    super(props)
    this.services = new GeneralServices()
    this.state = {
      isLoading: false,
      allCategories: [],
      allProducts: [],
      productsToShow: [],
      sortBy: '',
      editingCategory: false,
    }
  }

  componentWillMount() {
    const isSearching = searchWordOnServer()
    if (isSearching.isSearching === 'yes') {
      const word = isSearching.searchBy
      this.searchProducts(word)
    } else {
      this.getAllProducts()
    }
  }

  getAllProducts = () => {
    this.setState({
      isLoading: true,
    })
    this.services.getAllProducts().then((res) => {
      this.setState({
        isLoading: false,
        allProducts: res.data.getAllProducts,
        productsToShow: res.data.getAllProducts,
        allCategories: res.data.getAllCategories,
      })
    })
  }

  searchProducts = (word) => {
    localStorage.setItem('searchBy', '')
    localStorage.setItem('isSearching', '')
    this.setState({
      isLoading: true,
    })

    this.services.searchProducts(word).then((res) => {
      this.setState({
        isLoading: false,
        allProducts: res.data.getAllProducts,
        productsToShow: res.data.getSelectedProducts,
        allCategories: res.data.getAllCategories,
      })
    })
  }

  selectCategorie = (id) => {
    const products = this.state.allProducts
    this.setState({
      productsToShow: products.filter((product) => product.category === id),
      editingCategory: true,
    })
  }

  sortBy = (parameter) => {
    const newArraySorted = sortingBy(parameter, this.state.productsToShow)
    this.setState({
      productsToShow: newArraySorted,
      editingCategory: false,
    })
  }

  render() {
    return (
      <DivContainer>
        {this.state.isLoading ? (
          <IsLoading isLoading={this.state.isLoading} />
        ) : (
          <>
            <DivActions>
              <Categories
                allCategories={this.state.allCategories}
                selectCategorie={this.selectCategorie}
              />
              <SortBy
                sortBy={this.sortBy}
                editingCategory={this.state.editingCategory}
              />
            </DivActions>

            {this.state.productsToShow &&
            this.state.productsToShow.length > 0 ? (
              <ProductsToShow productsToShow={this.state.productsToShow} />
            ) : (
              <NotMatched />
            )}
          </>
        )}
      </DivContainer>
    )
  }
}
