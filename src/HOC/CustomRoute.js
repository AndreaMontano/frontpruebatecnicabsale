import React from 'react'
import { Route } from 'react-router-dom'

const CustomRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        const pushTo = (route) => {
          props.history.push(route)
        }

        return <Component pushTo={pushTo} {...props} />
      }}
    />
  )
}

export default CustomRoute
