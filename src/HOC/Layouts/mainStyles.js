import styled from 'styled-components'
import { Variables } from '../../assets/styles/variables'

export const Nav = styled.div`
  position: relative;
  margin: 0% !important;
`
export const MainDiv = styled.div`
  background-color: ${Variables.defaultWhiteColor};
  min-height: calc(100vh - 60px);
`
