import React from 'react'
import NavBar from './mainComponents/navBar/NavBar'

import { Nav, MainDiv } from './mainStyles'

const Main = (props) => (
  <div>
    <Nav className={`card`}>
      <NavBar />
    </Nav>

    <MainDiv>{props.children}</MainDiv>
  </div>
)

export default Main
