import React from 'react'
import { Link } from 'react-router-dom'
import { DivContainer, StoreName, PagesDiv, IconsDiv } from './navBarStyles'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import Popover from '../../../../views/homePublicPage/components/popover'

export default function navBar() {
  return (
    <DivContainer>
      <StoreName>Bsale Test</StoreName>
      <PagesDiv>
        <Link to={'/'}>Tienda</Link>
      </PagesDiv>
      <IconsDiv>
        <Popover
          searchWordOnServer={(event) => {
            localStorage.setItem('searchBy', event.target.value)
            localStorage.setItem('isSearching', 'yes')
            window.location.reload()
          }}
        />

        <Link to={'/'}>
          <ShoppingCartIcon />
        </Link>
      </IconsDiv>
    </DivContainer>
  )
}
