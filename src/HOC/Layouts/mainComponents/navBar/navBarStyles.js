import styled from 'styled-components'
import { Variables } from '../../../../assets/styles/variables'

export const DivContainer = styled.div`
  width: 100%;
  background-color: ${Variables.whiteColor};
  height: 60px;
  padding: 0 10%;
  display: flex;
  align-items: center;
  font-weight: bolder;
  margin: 0;

  @media (max-width: 599px) {
  }

  @media (min-width: 1280px) {
    width: 1100px;
    margin: auto;
    padding: 0;
  }
`

export const StoreName = styled.div`
  width: 20%;
  color: ${Variables.mediumGrayColor};
  height: 60px;
  display: flex;
  font-size: 1.2em;
  align-items: center;
`

export const PagesDiv = styled.div`
  width: 60%;
  height: 60px;
  display: flex;
  align-items: center;
  padding-left: 3%;
  a {
    color: ${Variables.mediumGrayColor};
  }
`

export const IconsDiv = styled.div`
  width: auto;
  height: 60px;
  display: flex;
  align-items: center;
  margin-left: auto;

  svg {
    color: ${Variables.mediumGrayColor};
  }
`
