import React, { Component } from 'react'
import MainLayout from './Main'

import CustomRoute from '../CustomRoute'
import { GetPublicPageRoute, PublicPageRoute } from '../../routes'
//Public Pages
import Home from '../../views/homePublicPage/Home'

export default class Dashboard extends Component {
  render() {
    return (
      <MainLayout>
        <CustomRoute isPrivate={false} path='/' exact component={Home} />
        {/*Public Pages*/}
        <CustomRoute
          isPrivate={false}
          path={GetPublicPageRoute(PublicPageRoute.home)}
          component={Home}
        />
      </MainLayout>
    )
  }
}
