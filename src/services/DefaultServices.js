import axios from 'axios'

class Services {
  constructor() {
    this.axios = axios
    axios.defaults.timeout = 1000 * 40
  }

  post(url, data, config) {
    console.log('url 🐒 post', url)
    return this.axiosPromise(() => this.axios.post(url, data, config))
  }

  put(url, data, config) {
    console.log('url 🐒 put', url)
    return this.axiosPromise(() => this.axios.put(url, data, config))
  }

  get(url, data, config) {
    console.log('url 🐒 get', url, this.axios.defaults.headers)
    return this.axiosPromise(() => this.axios.get(url, data, config))
  }

  download(url, data, config) {
    console.log('url 🐒 get', url)
    return this.axiosPromise(() => this.axios.get(url, data, config))
  }

  delete(url, data, config) {
    console.log('url 🐒 delete', url)
    const newConfig = {
      ...config,
      data,
    }
    return this.axiosPromise(() => this.axios.delete(url, newConfig))
  }

  axiosPromise(promise, retry = 0) {
    return new Promise((resolve, reject) => {
      promise()
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

export default Services
