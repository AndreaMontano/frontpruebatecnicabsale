import React, { Component } from 'react'
import { BrowserRouter } from 'react-router-dom'
import CustomRoute from './HOC/CustomRoute'
import 'materialize-css/dist/css/materialize.min.css'
import Dashboard from './HOC/Layouts/Dashboard'

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <CustomRoute isPrivate={false} path='/' component={Dashboard} />
      </BrowserRouter>
    )
  }
}
