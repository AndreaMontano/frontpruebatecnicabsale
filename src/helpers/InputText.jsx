import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'

import Styles from './inputTextStyles.module.scss'

//Reusable functional component

const useStyles = makeStyles((theme) => ({
  text: {
    '& > *': {
      margin: theme.spacing(0),
      width: '100%',
    },
  },
}))

export default function Input(props) {
  const classes = useStyles()

  return (
    <form
      className={`${classes.text} ${Styles.formInput}`}
      noValidate
      autoComplete='off'
    >
      <TextField
        type={props.type}
        className={`${Styles.textField}`}
        id='mui-theme-provider-outlined-input'
        variant='outlined'
        InputProps={{ inputProps: { min: props.min, max: props.max } }}
        name={props.name}
        defaultValue={props.defaultValue}
        label={props.placeholder}
        onKeyPress={(event) => {
          if (event.key === 'Enter' && !event.shiftKey) {
            if (event.target.value === '' || event.target.value === ' ') {
              window.alert('Debes ingresar texto a buscar y presionar enter')
            } else {
              props.onChange(event)
            }
          }
        }}
      />
    </form>
  )
}
