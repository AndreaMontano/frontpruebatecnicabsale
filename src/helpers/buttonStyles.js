import styled, { css } from 'styled-components'
import { Variables } from '../assets/styles/variables'

export const DivButton = styled.button`
  text-transform: uppercase;
  min-width: 105px !important;
  justify-content: space-around;
  display: flex;
  align-items: center;
  cursor: pointer;
  ${(props) =>
    css`
      background-color: ${!props.background
        ? Variables.grayColor
        : Variables[props.background]};
      color: ${Variables.whiteColor};
    `};

   span {
    width: auto !important;
    max-height: 100%
    margin: 2%;
    display: inline-table;
  }
`
