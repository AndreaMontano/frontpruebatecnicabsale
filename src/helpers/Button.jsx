import React from 'react'
import { DivButton } from '../helpers/buttonStyles'

//Reusable functional component, receives by properties: styles, placeholder and the onClick event

export default function Button(props) {
  return (
    <div>
      <DivButton
        className={`btn`}
        onClick={props.onClick}
        background={props.background}
      >
        <span>{props.text}</span>
      </DivButton>
    </div>
  )
}
