import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'

import Styles from './selectStyles.module.scss'

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(0),
    width: '100%',
  },
}))

export default function InputSelect(props) {
  const classes = useStyles()

  const inputLabel = React.useRef(null)
  const [labelWidth, setLabelWidth] = React.useState(0)
  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth)
  }, [])

  const [value, setValue] = useState('')

  return (
    <FormControl
      variant='outlined'
      className={`${classes.formControl} ${Styles.formControl}`}
    >
      <>
        <InputLabel ref={inputLabel} className={`${Styles.label}`}>
          {props.placeholder}
        </InputLabel>
        <Select
          className={`${Styles.select}`}
          name={props.name}
          onChange={(event) => {
            props.onChange(event.target.value)
            setValue(event.target.value)
          }}
          labelWidth={labelWidth}
          value={props.editingCategory ? '' : value}
        >
          {props.arrayOptions.map((option) => (
            <MenuItem value={option[props.keyValueName]}>
              {option[props.keyMessageName]}
            </MenuItem>
          ))}
        </Select>
      </>
    </FormControl>
  )
}
