import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import 'materialize-css/dist/css/materialize.min.css'
import App from './App'
import reportWebVitals from './reportWebVitals'

//We import the App component as the main module, which is the one that is rendered in the DOM

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)

reportWebVitals()
