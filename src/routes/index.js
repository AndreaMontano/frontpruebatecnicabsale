export const Base = {
  public: '/publicPages',
  views: '/views',
}

export const PublicPageRoute = {
  base: '/public-pages',
  home: '/home',
}

export const GetPublicPageRoute = (url) => {
  return Base.views + PublicPageRoute.base + url
}
