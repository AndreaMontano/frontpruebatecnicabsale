//Save the general styles that I import from all components

export const Variables = {
  mainColor: '#4DB6AC',
  mainSoftColor: '#f0f8ff',
  grayColor: '#424242',
  mediumGrayColor: '#8f8f8f',
  softGrayColor: '#e8e6e6',
  ultraSoftGrayColor: '#eff1fb',
  ultraUltraSoftGrayColor: '#f1f1f1',
  defaultWhiteColor: '#f1f1f1',
  whiteColor: 'white',
  defaultRedColor: '#722F38',
  fontTitles: "'Caveat', cursive",
}
